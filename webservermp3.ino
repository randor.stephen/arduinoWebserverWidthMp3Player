#include <DFPlayer_Mini_Mp3.h>    //library voor de mp3 player module commands
#include <SD.h>                   //library voor het comuniceren met de sd kaart
#include <LiquidCrystal.h>        //library voor de cd display van 2x16
#include <SPI.h>                  //library voor verbinding van het ethernet shield met arduino uno
#include <Ethernet.h>             //library voor verbinding van ethernet shield met het netwerk

//8.1
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };  //mac adres voor arduino in array van 6 bytes

EthernetServer server(80);            //poort voor verbinding 80 is standaart voor http
LiquidCrystal lcd(8, 9, 5, 7, 3, 2);  //maak lcd object met alle pins voor het lcd
File bestand;                         //maak variabele waar het index.htm bestand in word geopend

boolean playing = false;  //boolean voor of we doorwillen gaan met volgende numer
String isCmd = "";        //voor alle ontvangen http data van client
boolean sendStat = false; //boolean voor of we ajax gaan stuuren of niet
int volume = 8;           //int voor bijhouden van volume van mp3
String song = "";
int playNext = 0;

void setup() {
  
  Serial.begin(9600);       // begin seriele verbinding
  Ethernet.begin(mac);      // begin de verbinding 
  server.begin();           // begin de server
  lcd.begin(16, 2);         // geef lcd object de lcd groote
  mp3_set_serial(Serial);   // set seriele conectie met mp3 player
  delay(1);                 // zodat het volume goed word gezet
  mp3_set_volume(14);        // stel het volume in 0 t/m 30
  delay(20);

  //laat volume zien en adres van mp3 speler
  lcd.print("volume: 8 adres:");
  lcd.setCursor(0, 1);      //ga naar volgende regel
  lcd.print(Ethernet.localIP());

  SD.begin(4);              // begin de sd kaart verbinding op pin 4 met ethernet shield
}

void loop() {
  EthernetClient client = server.available();   //sla huidige client connectie status op
  checkPlayState();
 
  if (client) {    //kijk of er echt een client is
    boolean huidigeRegelIsBlank = true; //voor de huidige regel van client
    
    while(client.connected()) {       //zolang de client verbonden is
      if(client.available()) {          //kijk of de client data toegankelijk is
        char userData = client.read();    //haal 1byte op van de verbonde gebruiker/client
        
        //laatste lijn die we krijgen van de client is wit en eindigt met \n 
        //daarna willen wij beginen met het stuuren van de pagina
        if(userData == '\n' && huidigeRegelIsBlank) {
          
          //stuur http reactie
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");   //content-type is text html het soort pagina die we stuuren
          client.println("Connection: keep-alive");   //disconect niet de client 
          client.println();   //witregel

          if(!sendStat) {
            bestand = SD.open("index.htm");     //open de index pagina

            if(bestand) {             //als bestand een waarde bevat
              while(bestand.available()) {    //zolang index.htm beschikbaar is
                client.write(bestand.read()); //stuur index.htm naar de client
            }
            bestand.close();        //sluit het bestand weer
          }
          break;    //stop 
          } else if(sendStat) {
            client.print("Mp3 volume: ");
            client.print(volume);
            client.print("  status: ");
            if(playing) {
              client.println("aan het afspelen!");
            } else {
              client.println("gepauzeerd!"); 
            }
            sendStat = false;
            break;
          }
        }
        //alle regels die we krijgen van client eindigt op \r\n
        //check alle regels voor tekst
        if(userData == '\n') {
          
          //als \n het einde was
          //huidige regel is blank dus
          huidigeRegelIsBlank = true;
         
        } else if(userData != '\r') {
          
          //als einde van regel van client niet eindigt op \r\n
          //is er een text karakter ontvangen dus regel is niet blank
          huidigeRegelIsBlank = false;
          
        }     
        //functie die checked voor een comand 
        //param1 = byte die we krijgen param2 = client waar we naar schrijven
        checkDataForCmd(userData);
      }
    }
    delay(1);       //geef browser tijd om data te ontvangen
    client.stop();  //stop de verbiding met de client
  }
}

//check if we need to play next song
void checkPlayState() {
  if(playing) {   //check of we ook wat willen afspeelen
    delay(50);  //wacht ff voordat we beginen met lezen van pin
    int afspeelStaat = digitalRead(6); //lees busy pin waarde 1 of 0
    if(afspeelStaat == 1) { //als speelstatus van busy pin 1 is dus niks speelt
      playNext += 1;
      if(playNext == 8 && afspeelStaat == 1) {
          mp3_next();   //speelvolgende nummer
          playing = true;
          showValues();
          delay(300);   // tijd om comand te verwerken
          playNext = 0;
      } else {
        playNext = 0;
      }
    }
  }
}

//functie die get requests filtert voor comands 
//param1= bytes van http get request param2= ethernet client waar we naar schrijven
void checkDataForCmd(char data) {
  String Data = isCmd + data; //om data samen te voegen voor comand
  
  if(Data == "T") {             //alle get comands vinden we door de T van GET anders word de comand 2x gevonden
    isCmd = Data;
    Data = "";
    
  } else if(Data == "T ") {     //daarna komt altijd een spatie
    isCmd = Data;
    Data = "";
    
  } else if(Data == "T /") {    //daarna komt altijd een /
    isCmd = Data;
    Data = "";
    
  } else if(Data == "T /1") {     //daarna komt voor een ajax request een 1 van begin adres
    sendStat = true;      //stuur bericht var is true 
    isCmd = "";  //comand is gevonden dus maak variabele leeg
    Data = "";   //maak data leeg om nieuwe comands te vinden
  } else if (Data == "T /?") {    //begin va elk get comand voor input request <><>
    isCmd = Data;
    Data = "";
    
  } else if (Data == "T /?n") {  //next
    isCmd = "";
    mp3_next();
    playing = true;
    showValues();
    delay(600);
    
  } else if(Data == "T /?p") {  //previous
    isCmd = "";
    mp3_prev();
    playing = true;
    showValues();
    delay(600);
    
  } else if (Data == "T /?a") {  //afspelen
    isCmd = "";
    mp3_play();
    playing = true;
    showValues();
    delay(600);
    
  } else if (Data == "T /?r") {  //Random play all songs ___________
    isCmd = "";
    mp3_next();
    delay(600);
    
  } else if(Data == "T /?P") {  //pauze
    isCmd = "";
    playing = false;
    mp3_pause();
    showValues();
    delay(600);
    
  } else if (Data == "T /?i") {  //volume+
    isCmd = "";
    volume += 1;
    mp3_set_volume(volume);
    showValues();
    delay(600);
    
  } else if (Data == "T /?d") {  //volume-
    isCmd = "";
    volume -= 1;
    mp3_set_volume(volume);
    showValues();
    delay(600);
    
  } else if(Data == "T /?l") {  //replay this current song <><>x1 __________
    isCmd = "";
    mp3_play();
    delay(600);
    
  } else {
    Data = "";
    isCmd = "";
  }
}

void showValues() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("volume: ");
  lcd.print(volume);
  //haal song naam op en sla op in song
  mp3_get_state();
  //print song naam op scherm
  lcd.setCursor(10, 0);
  lcd.print("numr:");
  lcd.setCursor(0, 1);
  if(playing) {
    lcd.print("aan het afspelen!");
  } else {
    lcd.print("gepauzeerd!"); 
  }
}

